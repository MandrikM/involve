import React, { Component } from 'react'
import SellBuy from './SellBuy'
import { loadMethods} from './actions'
import {connect} from "react-redux";
import {  Link } from "react-router-dom";


class MainPage extends Component {
    constructor(props) {
        super(props);
        this.props.loadMethods("LOAD METHOD")
    }
    render () {

        return (
            <div className='MainPage' >
                <SellBuy type = "Sell" allMethodes="invoice"/>
                <SellBuy type = "Buy" allMethodes="withdraw"/>
                <div className="Exchange">
                    <button><div><Link
                    to={{ pathname: "/info",
                        state: this.props.state  }}  >Exchange</Link></div>
                </button>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({ state: state })
export default connect(mapStateToProps, { loadMethods })(MainPage)

