import React, { Component } from 'react'
import success from '../media/success.svg'

class Success extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange (event) {
        window.location.href = "/"
        event.preventDefault()
    }
    render () {
        return (
            <div className="MainPage">
                <img src={success} alt="Success" className="img"/>
                <div className="SuccessText">
                    <h1>Success!</h1>
                    <div>Your exchange order has been placed successfully and will be processed soon.</div>
                </div>
                <button onClick={this.handleChange} className="SuccessButt">Home</button>
            </div>
        )
    }
}

export default Success
