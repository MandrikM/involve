import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import MainPage from './MainPage';
import Info from './Info';
import Success from './Success';

function App() {
    return (
        <Router>
            <div >
                    <Route exact  path="/" component={MainPage} />
                    <Route path="/info" component={Info} />
                    <Route path="/success" component={Success} />
                </div>
        </Router>
    );
}

export default App;
