import React, { Component } from 'react'
import { connect } from 'react-redux'
import {Confirm,loadMethods} from "./actions";
import loader from "../media/loader.svg";

class Info extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
    }
    handleChange (event) {
        this.props.loadMethods("LOADING");
        this.props.Confirm(this.props.state[this.props.state.base].amount,this.props.state.base,this.props.state.invoice.cur.id, this.props.state.withdraw.cur.id)
        event.preventDefault()
    }
    handleCancel (event) {
        window.location.href = "/"
        event.preventDefault()
    }
    render () {
        return (
            <div className="MainPage" >
                <label className="details">Details</label>
                <div className="Div">
                    <div className="Text2" >Sell</div>
                    <div  className="Text2">Buy</div>
                </div>
                   <div className="info"><div  className="Text1">{this.props.state.invoice.amount} {this.props.state.invoice.cur.name}</div>
                       <div  className="Text1"> {this.props.state.withdraw.amount} {this.props.state.withdraw.cur.name}</div>
                    </div>
                <button onClick={this.handleCancel} className="Cancel">Cancel</button>
                <button onClick={this.handleChange} className="Confirm">  {this.props.state.loading ?  <img src={loader} alt="Loader" /> : <div>Confirm</div>}</button>
            </div>
        )
    }
}
const mapStateToProps = state => ({ state: state })
export default connect(mapStateToProps,{Confirm,loadMethods})(Info);
