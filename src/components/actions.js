export function changeParam (operationType, value, type, base,invoiceId, withdrawId, amount) {
    return {
            type:type,
            operationType:operationType,
            value: value,
            base : base,
            amount: amount,
            invoiceId:invoiceId,
            withdrawId:withdrawId
    }
}

export function changeAmount (operationType, value, type,sellId,buyId) {
    return {
        type: type,
        operationType:operationType,
        value:value,
        invoiceId:sellId,
        withdrawId:buyId
    }
}
export  function loadMethods(type) {
    return{
        type:type
    }
}
export  function allMethodes(methods) {
    return{
        type:'ALL METHODES',
        all: methods
    }
}
export  function InfoData(invoice,withdraw,base) {
    return{
        type:'INFO DATA',
        invoice: invoice,
        withdraw : withdraw,
        base: base
    }
}
export function Confirm(amount,base,invoicePayMethod,withdrawPayMethod) {
    return{
        type: 'CONFIRM',
        amount: parseInt(amount) ,
        base: base,
        invoicePayMethod:  parseInt(invoicePayMethod),
        withdrawPayMethod:  parseInt(withdrawPayMethod)

    }
}

export function ChangeParamWithAmount(operationType,value,invoiceAmount,withdrawAmount) {
    return{
        type:'PARAM WITH AMOUNT',
        operationType:operationType,
        value:value,
        invoiceAmount: invoiceAmount,
        withdrawAmount : withdrawAmount

    }
}