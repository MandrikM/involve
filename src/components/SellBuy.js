import React, { Component } from 'react'
import { connect } from 'react-redux'
import { changeParam,loadMethods ,changeAmount } from './actions'
import loader from "../media/loader.svg";

class SellBuy extends Component {
    constructor(props) {
        super(props);
        this.state={
            type: this.props.type,
            allMethodes: this.props.allMethodes,
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleChangeText = this.handleChangeText.bind(this)
    }
    handleChange (event) {
        let CurMet = this.props.state[this.state.allMethodes].arr.find(obj => obj.id == event.target.value)
        if (this.props.state.base == ""){
            this.props.changeParam(
                this.state.allMethodes,
                CurMet,
                "CHANGE PARAM")
        }else{
            this.props.loadMethods("LOADING");
            if( this.state.allMethodes == "invoice"){
                this.props.changeParam(
                    this.state.allMethodes,
                    CurMet,
                    "CHANGE PARAM WITH AMOUNT" ,
                    this.props.state.base,
                    event.target.value,
                    this.props.state.withdraw.cur.id ,
                    this.props.state[this.props.state.base].amount)
            } else{
                this.props.changeParam(
                    this.state.allMethodes,
                    CurMet,
                    "CHANGE PARAM WITH AMOUNT" ,
                    this.props.state.base,
                    this.props.state.invoice.cur.id ,
                    event.target.value,
                    this.props.state[this.props.state.base].amount)
            }
        }
        event.preventDefault()
    }

    handleChangeText (event) {
        this.props.loadMethods("LOADING");
        this.props.changeAmount(this.state.allMethodes,event.target.value,"PUT AMOUNT",this.props.state.invoice.cur.id,this.props.state.withdraw.cur.id)
        event.preventDefault()
    }

    render () {
        return (
            <div  className={this.state.type}>
                <form>
                    <div><label>{this.state.type}</label></div>
                    <select onChange={this.handleChange} value={this.props.state[this.state.allMethodes].cur.id}>
                        {this.props.state[this.state.allMethodes].arr.map((meth) =>{ if (meth == this.props.state[this.state.allMethodes].cur){
                            return <option key={meth.id} value={meth.id} selected>{meth.name}</option>
                        }else{ return <option key={meth.id} value={meth.id}>{meth.name}</option>}} )}
                    </select>
                    <div>
                        <input type="text" onChange={this.handleChangeText} value={this.props.state[this.state.allMethodes].amount}/>
                        {this.props.state.loading ?  <img src={loader} alt="Loader" /> : <div></div>}
                    </div>
                </form>

            </div>
        )
    }
}
const mapStateToProps = state => ({ state: state })
export default connect(mapStateToProps , { changeParam,loadMethods , changeAmount})(SellBuy)
