export default function (state = {
    invoice: {arr:[], cur: {},amount:""},
    withdraw:{arr:[], cur: {},amount:""},
                             base:"",
                         loading:false}
    , action) {
    let newState

    switch (action.type) {
        case "ALL METHODES":
            newState = {invoice:{arr:action.all.invoice, cur: action.all.invoice[0],amount:""},
                withdraw:{arr:action.all.withdraw, cur: action.all.withdraw[0],amount:""},
                base:"",
                loading:false};
            return newState
        case 'CHANGE PARAM':
            if(action.operationType === "invoice"){
                newState = {invoice:{arr:state.invoice.arr, cur:action.value,amount:""},
                    withdraw:{arr:state.withdraw.arr,cur:state.withdraw.cur,amount:""},
                    base:"",
                    loading:false};
            }else {
                newState = {invoice:{arr:state.invoice.arr,cur:state.invoice.cur,amount:""},
                    withdraw:{arr:state.withdraw.arr, cur:action.value,amount:""},base:"",loading:false};
            }
            return newState;
        case 'INFO DATA' :

            newState = {invoice:{arr:state.invoice.arr,cur:state.invoice.cur,amount:action.invoice},
                withdraw:{arr:state.withdraw.arr, cur:state.withdraw.cur,amount:action.withdraw},
                base:action.base,
                loading:false};

            return newState
        case 'LOADING':
            return {
                invoice: state.invoice,
                withdraw: state.withdraw,
                base:state.base,
                loading:true}
        case "PARAM WITH AMOUNT":{
            newState ={invoice:{arr:state.invoice.arr,cur:state.invoice.cur,amount:action.invoiceAmount},
                withdraw:{arr:state.withdraw.arr, cur:state.withdraw.cur,amount:action.withdrawAmount},
                base:state.base,
                loading:false}
                newState[action.operationType].cur=action.value
            return newState
        }
        default:
            return state
    }
}
