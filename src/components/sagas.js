import { all, put, takeEvery } from 'redux-saga/effects'

import { allMethodes, InfoData,ChangeParamWithAmount } from './actions'

function * LoadMethods () {
    yield takeEvery('LOAD METHOD', Loadsmth)
}

function * Loadsmth () {
    const methodes = yield fetch("https://involve-it.com/test_front/api/payMethods")
       .then(response => response.json())
    yield put(allMethodes(methodes))
}
function * Amount () {
    yield takeEvery("PUT AMOUNT", AmountV)
}

function * AmountV (action) {
    let url = new URL('https://involve-it.com/test_front/api/payMethods/calculate')

    let params = {base:action.operationType,amount: parseInt(action.value||0),
        invoicePayMethod:parseInt(action.invoiceId),
        withdrawPayMethod: parseInt(action.withdrawId)}
    url.search = new URLSearchParams(params).toString();

    const amount = yield fetch(url)
        .then(response => response.json())
    if(amount.message){
        yield put(InfoData("","",action.operationType))
    }else{
        if(action.operationType === "invoice"){
            yield put(InfoData(action.value,amount.amount,action.operationType))
        }else {
            yield put(InfoData(amount.amount,action.value,action.operationType))
        }
    }

}
function * Confirm () {
    yield takeEvery('CONFIRM', ConfirmChange)
}

function * ConfirmChange (action) {
    const res = yield fetch('https://involve-it.com/test_front/api/bids', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            amount: action.amount ||0,
            base: action.base ,
            invoicePayMethod: action.invoicePayMethod,
            withdrawPayMethod: action.withdrawPayMethod
        })
    })
        .then(response => response.json())
    if(res.message ==='Success' ){
        window.location.href = "/success";
    }
}
function * ChangeWithAmount () {
    yield takeEvery("CHANGE PARAM WITH AMOUNT", ChangeWithA)
}

function * ChangeWithA (action) {
    let url = new URL('https://involve-it.com/test_front/api/payMethods/calculate')
    let params = {base:action.base,amount: parseInt(action.amount||0),
        invoicePayMethod:parseInt(action.invoiceId),
        withdrawPayMethod: parseInt(action.withdrawId)}
    url.search = new URLSearchParams(params).toString();

    const amount = yield fetch(url)
        .then(response => response.json())
    if(amount.message){
        yield put(ChangeParamWithAmount(action.operationType,action.value,"",""))
    }else{
        if(action.base === "invoice"){
            yield put(ChangeParamWithAmount(action.operationType,action.value,action.amount,amount.amount))
        }else {
            yield put(ChangeParamWithAmount(action.operationType,action.value,amount.amount,action.amount))
        }
    }

}

export default function * watchAll () {
    yield all([
        LoadMethods(),
        Amount(),
        Confirm(),
        ChangeWithAmount(),
    ])
}
