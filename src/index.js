import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import reducer from './components/reducer'
import createSagaMiddleware from 'redux-saga'
import watchAll from './components/sagas'
import './media/App.css'
import { Router } from "react-router";
import { createBrowserHistory } from "history";

const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(watchAll)
const history = createBrowserHistory();
function render () {
    ReactDOM.render(
        <Provider store={store}>
            <Router history={history}>
                <App />
            </Router>

        </Provider>,
        document.getElementById('root')
    )
}

render()
store.subscribe(render)
